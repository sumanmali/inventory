<?php

namespace App\Http\Controllers;

use App\Investor;
use Illuminate\Http\Request;

class InvestorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lot = session()->get('lot');
        $investors = Investor::where('lot_id', $lot->id)->get();
        return view('backend.investor.index', compact('investors', 'lot'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $lot = session()->get('lot');
        return view('backend.investor.create', compact('lot'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Investor::create($request->all());
        return redirect('investor');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Investor  $investor
     * @return \Illuminate\Http\Response
     */
    public function show(Investor $investor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Investor  $investor
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $lot = session()->get('lot');
        $investor = Investor::findOrFail($id);
        return view('backend.investor.edit', compact('investor', 'lot'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Investor  $investor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Investor::findOrFail($id)->update($request->all());
        return redirect('/investor')->with('success', 'Investor Details has been updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Investor  $investor
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Investor::findOrFail($id)->delete();
        return redirect('/investor')->with('success', 'Investor has been deleted successfully.');
    }
}
