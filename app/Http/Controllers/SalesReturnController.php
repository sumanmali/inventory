<?php

namespace App\Http\Controllers;

use App\SalesReturn;
use Illuminate\Http\Request;
use App\Sales;
use App\Stock;

class SalesReturnController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lot = session()->get('lot');
        $sales = Sales::all();
        $sales_returns = SalesReturn::where('lot_id', $lot->id)->get();
        return view('backend.salesReturn.index', compact('sales', 'sales_returns'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sales = Sales::where('id', $request->sales_id)->first();
        $stock = Stock::where('model_no', $sales->model_no)->first();
        $stock->update([
            'stock' => $stock->stock + $request->quantity
        ]);
        $salesReturn = SalesReturn::create($request->all());
        if ($salesReturn->wasRecentlyCreated) {
            return redirect('/sales')->with('success', 'A new sales return is added successfully.');
        }
        return redirect('/sales')->with('error', 'Error on sales return entry.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SalesReturn  $salesReturn
     * @return \Illuminate\Http\Response
     */
    public function show(SalesReturn $salesReturn)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SalesReturn  $salesReturn
     * @return \Illuminate\Http\Response
     */
    public function edit(SalesReturn $salesReturn)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SalesReturn  $salesReturn
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SalesReturn $salesReturn)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SalesReturn  $salesReturn
     * @return \Illuminate\Http\Response
     */
    public function destroy(SalesReturn $salesReturn)
    {
        //
    }
}