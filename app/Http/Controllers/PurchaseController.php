<?php

namespace App\Http\Controllers;

use App\Purchase;
use App\Vendor;
use Illuminate\Http\Request;

use App\Services\RemainingAmountCalc;
use App\Services\StockMaintainer;

class PurchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $remainingAmtService, $stockMaintain;

    public function __construct(RemainingAmountCalc $amount, StockMaintainer $stockMaintain)
    {
        $this->remainingAmtService = $amount;
        $this->stockMaintain = $stockMaintain;
    }

    public function index()
    {
        $lot = session()->get('lot');
        $purchases = Purchase::where('lot_id', $lot->id)->orderBy('id', 'DESC')->get();
        $vendors = Vendor::all();
        return view('backend.purchase.index', compact('purchases', 'lot', 'vendors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $lot = session()->get('lot');
        $vendors = Vendor::all();
        $canPurchase = $this->remainingAmtService->lotWiseCalculator() > 0;

        return view('backend.purchase.create', compact('lot', 'vendors', 'canPurchase'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $stocker = $this->stockMaintain->stockMaintainOnPurchase($request->model_no, $request->quantity, $request->size);
        if ($stocker) {
            $purchase = Purchase::create($request->all());
            if ($purchase->wasRecentlyCreated) {
                return redirect('/purchase')->with('success', 'A new purchase is added successfully.');
            }
            return redirect('/purchase')->with('error', 'Error on purchase entry.');
        }
        return redirect('/purchase')->with('error', 'Purchase Failed!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function show(Purchase $purchase)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function edit(Purchase $purchase)
    {
        $lot = session()->get('lot');
        $vendors = Vendor::all();
        return view('backend.purchase.edit', compact('purchase', 'vendors', 'lot'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Purchase $purchase)
    {
        if ($purchase->quantity > $request->quantity) {
            $stocker = $this->stockMaintain->stockMaintainOnSales($purchase->model_no, $purchase->quantity - $request->quantity);
            if ($stocker) {
                $this->updatepurchasesRecord($purchase, $request);
            }
            return redirect('/purchases')->with('error', 'Error on purchases record update.');
        } elseif ($purchase->quantity < $request->quantity) {
            $stocker = $this->stockMaintain->stockMaintainOnPurchase($purchase->model_no, $request->quantity - $purchase->quantity);
            if ($stocker) {
                $this->updatepurchasesRecord($purchase, $request);
            }
            return
                redirect('/purchases')->with('error', 'Error on purchases record update.');
        } else {
            $this->updatepurchasesRecord($purchase, $request);
        }
        return redirect('/purchases')->with('error', 'Error on purchases record update.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function destroy(Purchase $purchase)
    {
        $stocker = $this->stockMaintain->stockMaintainOnSales($purchase->model_no, $purchase->quantity);
        if ($stocker) {
            $purchase->delete();
            return redirect('/purchase')->with('success', 'Purchase Record Deleted!');
        }
        return redirect('/purchase')->with('error', 'Error on purchase record deletion!');
    }

    public function updatepurchasesRecord($purchase, $request)
    {
        $purchase->update($request->all());
        return redirect('/purchases')->with('success', 'An existing purchases record has been updated!');
    }
}