<?php

namespace App\Http\Controllers;

use App\Purchase;
use App\Investor;
use App\Expenses;
use App\User;
use App\Sales;
use App\Stock;
use App\Lot;
use App\Services\RemainingAmountCalc;
use App\Services\TotalPurchase;
use App\Services\TotalSales;

use Illuminate\Http\Request;

class LotController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $remainingAmtService, $totalPurchase, $totalSales;

    public function __construct(RemainingAmountCalc $amount, TotalPurchase $totalPurchase, TotalSales $totalSales)
    {
        $this->remainingAmtService = $amount;
        $this->totalPurchase = $totalPurchase;
        $this->totalSales = $totalSales;
    }

    public function index()
    {
        $lot = session()->get('lot');
        $purchases = Purchase::orderBy('id', 'DESC')->get();
        $saleses = Sales::orderBy('id', 'DESC')->get();
        $investors = Investor::where('lot_id', $lot->id)->get();
        $expenses = Expenses::where('lot_id', $lot->id)->get();

        $totalInvestment = Investor::where('lot_id', $lot->id)->sum('amount');
        $totalExpenses = Expenses::where('lot_id', $lot->id)->sum('amount');

        $totalPurchase = $this->totalPurchase->returnTotalPurchase($purchases);
        $totalSalesArr = $this->totalSales->returnTotalSales($saleses);

        $remainingAmt = $this->remainingAmtService->lotWiseCalculator();

        $remainingAmt = round($remainingAmt, 2);
        $sessionName = 'investmentForNextLot' . $lot->id;
        session()->put($sessionName, [
            'lot' => $lot->id,
            'amt' => $remainingAmt
        ]);

        print_r(session()->get(
            $sessionName
        ));
        print_r($sessionName);
        return view('backend.dashboard', compact(
            'purchases',
            'saleses',
            'investors',
            'expenses',
            'totalInvestment',
            'totalExpenses',
            'totalPurchase',
            'totalSalesArr',
            'remainingAmt'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $lots = Lot::all();
        $counter = 0;
        foreach ($lots as $lot) {
            if ($lot->name == $request->name) {
                $counter += 1;
            }
        }
        if ($counter > 0) {
            return redirect('/home')->with('lots');
        }
        $lot = Lot::create($request->all());
        $lots = Lot::all();
        return redirect('/home')->with('lots');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Lot  $lot
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $lot = Lot::findOrFail($id);
        session()->put('lot', $lot);
        return $this->index();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Lot  $lot
     * @return \Illuminate\Http\Response
     */
    public function edit(Lot $lot)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Lot  $lot
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Lot $lot)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Lot  $lot
     * @return \Illuminate\Http\Response
     */
    public function destroy(Lot $lot)
    {
        //
    }
}