<?php

namespace App\Http\Controllers;

use App\Expenses;
use App\Purchase;
use App\Investor;

use Illuminate\Http\Request;

class ExpensesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lot = session()->get('lot');
        $expenses = Expenses::where('lot_id', $lot->id)->get();
        return view('backend.expenses.index', compact('lot', 'expenses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $lot = session()->get('lot');
        $purchases = Purchase::where('lot_id', $lot->id)->get();
        
        $totalPurchase = 0;
        foreach($purchases as $purchase){
            $totalPurchase += $purchase->quantity * $purchase->price;
        }
        $amount = Investor::where('lot_id', $lot->id)->sum('amount');
        $canMakeExpenses = $amount - $totalPurchase;
        return view('backend.expenses.create', compact('lot', 'canMakeExpenses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Expenses::create($request->all());
        return redirect('expenses')->with('success', 'A new expenses record is added successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Expenses  $expenses
     * @return \Illuminate\Http\Response
     */
    public function show(Expenses $expenses)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Expenses  $expenses
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $expense = Expenses::findOrFail($id);
        $lot = session()->get('lot');
        return view('backend.expenses.edit', compact('expense', 'lot'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Expenses  $expenses
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $id)
    {
        $expense = Expenses::findOrFail($id)->update($request->all());
        return redirect('expenses')->with('success', 'An expense record has been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Expenses  $expenses
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id)
    {
        Expenses::findOrFail($id)->delete();
        return redirect('expenses')->with('success', 'An expense record has been deleted!');
    }
}