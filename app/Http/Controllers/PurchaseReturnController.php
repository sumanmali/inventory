<?php

namespace App\Http\Controllers;

use App\PurchaseReturn;
use App\Purchase;
use App\Stock;
use Illuminate\Http\Request;

class PurchaseReturnController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lot = session()->get('lot');
        $purchase_returns = PurchaseReturn::where('lot_id', $lot->id)->get();
        $purchases = Purchase::where('lot_id', $lot->id)->get();
        return view('backend.purchaseReturn.index', compact('purchase_returns', 'purchases'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $purchase = Purchase::where('id', $request->purchase_id)->first();
        $stock = Stock::where('model_no', $purchase->model_no)->first();
        $stock->update([
            'stock' => $stock->stock - $request->quantity
        ]);
        $purchaseReturn = PurchaseReturn::create($request->all());
        if($purchaseReturn->wasRecentlyCreated){
            return redirect('/purchase')->with('success', 'A new purchase return is added successfully.');
        }
        return redirect('/purchase')->with('error', 'Error on purchase return entry.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PurchaseReturn  $purchaseReturn
     * @return \Illuminate\Http\Response
     */
    public function show(PurchaseReturn $purchaseReturn)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PurchaseReturn  $purchaseReturn
     * @return \Illuminate\Http\Response
     */
    public function edit(PurchaseReturn $purchaseReturn)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PurchaseReturn  $purchaseReturn
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PurchaseReturn $purchaseReturn)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PurchaseReturn  $purchaseReturn
     * @return \Illuminate\Http\Response
     */
    public function destroy(PurchaseReturn $purchaseReturn)
    {
        //
    }
}
