<?php

namespace App\Http\Controllers;

use App\Stock;
use App\Lot;


use App\Services\ProfitCalc;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    protected $profitCalc;

    public function __construct(ProfitCalc $profitCalc)
    {
        $this->middleware('auth');
        $this->profitCalc = $profitCalc;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $lots = Lot::all();
        return view('home', compact('lots'));
    }

    public function profitLoss()
    {
        $lotProfit = $this->profitCalc->lotProfit();
        $netProfit = $this->profitCalc->netProfit();
        return view('backend.profitLoss.index', compact('lotProfit', 'netProfit'));
    }

    public function stock()
    {
        $overall_stocks = Stock::all();
        return view('backend.stock.index', compact('overall_stocks'));
    }
}