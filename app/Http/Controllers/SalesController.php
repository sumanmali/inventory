<?php

namespace App\Http\Controllers;

use App\Sales;
use App\Stock;
use App\Customer;
use App\Services\StockMaintainer;

use Illuminate\Http\Request;

class SalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $stockMaintain;

    public function __construct(StockMaintainer $stockMaintain)
    {
        $this->stockMaintain = $stockMaintain;
    }

    public function index()
    {
        $lot = session()->get('lot');
        $sales = Sales::all();
        $customers = Customer::all();
        return view('backend.sales.index', compact('sales', 'lot', 'customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $lot = session()->get('lot');
        $stocks = Stock::where('stock', '>=', 1)->get();
        $customers = Customer::all();
        return view('backend.sales.create', compact('lot', 'stocks', 'customers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $stocker = $this->stockMaintain->stockMaintainOnSales($request->model_no, $request->quantity);
        if ($stocker) {
            $sales = Sales::create($request->all());
            if ($sales->wasRecentlyCreated) {
                return redirect('/sales')->with('success', 'New sales record is entered!');
            }
            return redirect('/sales')->with('error', 'Error on entering sale record.');
        }
        return redirect('/sales')->with('error', 'Out of Stock!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sales  $sales
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sales  $sales
     * @return \Illuminate\Http\Response
     */
    public function edit(Sales $sale)
    {
        $customers = Customer::all();
        $stocks = Stock::all();
        return view('backend.sales.edit', compact('sale', 'stocks', 'customers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sales  $sales
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sales $sale)
    {
        if ($sale->quantity > $request->quantity) {
            $stocker = $this->stockMaintain->stockMaintainOnPurchase($sale->model_no, $sale->quantity - $request->quantity);
            if ($stocker) {
                $this->updateSalesRecord($sale, $request);
            }
            return redirect('/sales')->with('error', 'Error on sales record update.');
        } elseif ($sale->quantity < $request->quantity) {
            $stocker = $this->stockMaintain->stockMaintainOnSales($sale->model_no, $request->quantity - $sale->quantity);
            if ($stocker) {
                $this->updateSalesRecord($sale, $request);
            }
            return
                redirect('/sales')->with('error', 'Error on sales record update.');
        } else {
            $this->updateSalesRecord($sale, $request);
        }
        return redirect('/sales')->with('error', 'Error on sales record update.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sales  $sales
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sales $sale)
    {
        $stocker =
            $this->stockMaintain->stockMaintainOnPurchase($sale->model_no, $sale->quantity);
        if ($stocker) {
            $sale->delete();
            return redirect('/sales')->with('success', 'An sales record has been deleted successfully!');
        }
        return redirect('/sales')->with('error', 'Error on sales record delete.');
    }

    public function updateSalesRecord($sale, $request)
    {
        $sale->update($request->all());
        return redirect('/sales')->with('success', 'An existing sales record has been updated!');
    }
}