<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sales extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'model_no', 'customer_id', 'bill_no', 'quantity', 'price', 'sales_date', 'mode',
    ];
}