<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Purchase extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'model_no', 'lot_id', 'vendor_id', 'receipt_no', 'quantity', 'price', 'size', 'brand', 'purchase_date'
    ];
}