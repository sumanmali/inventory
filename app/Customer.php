<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = [
        'company_name', 'PAN', 'customer', 'location', 'customer_phone', 'points'
    ];
}