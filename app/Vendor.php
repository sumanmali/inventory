<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    protected $fillable = [
        'company_name', 'location', 'PAN',  'contact_person', 'contact_person_phone', 'lot_id'
    ];
}