<?php

namespace App\Services;

use App\Purchase;
use App\PurchaseReturn;
use App\Sales;
use App\SalesReturn;
use App\Expenses;

use App\Services\TotalPurchase;
use App\Services\TotalPurchaseReturn;
use App\Services\TotalSales;
use App\Services\TotalSalesReturn;

class ProfitCalc
{

    protected $lot, $totalPurchase, $totalSales, $salesReturns, $purchaseReturns, $totalSalesArr,
        $purchases, $sales, $expenses, $totalExpenses, $totalPurchaseReturns, $profit;

    public function __construct(
        TotalPurchase $totalPurchase,
        TotalSales $totalSales,
        TotalPurchaseReturn $totalPurchaseReturns,
        TotalSalesReturn $totalSalesReturn
    ) {
        $this->totalPurchase = $totalPurchase;
        $this->totalSales = $totalSales;
        $this->totalPurchaseReturns = $totalPurchaseReturns;
        $this->totalSalesReturn = $totalSalesReturn;
    }

    public function lotProfit()
    {
        $this->lot = session()->get('lot');
        $this->purchases = Purchase::where('lot_id', $this->lot['id'])->get();
        $this->sales = Sales::all();
        $this->expenses = Expenses::where('lot_id', $this->lot['id'])->get();
        $this->totalExpenses = Expenses::where('lot_id', $this->lot['id'])->sum('amount');
        $this->salesReturns = SalesReturn::where('lot_id', $this->lot['id'])->get();
        $this->purchaseReturns = PurchaseReturn::where('lot_id', $this->lot['id'])->get();

        $totalPurchase = $this->totalPurchase->returnTotalPurchase($this->purchases);
        $totalSalesArr = $this->totalSales->returnTotalSales($this->sales);

        $totalPurchaseReturns = $this->totalPurchaseReturns->returnTotalPurchaseReturn($this->purchaseReturns, $this->purchases);
        $totalSalesReturn = $this->totalSalesReturn->returnTotalSalesReturn($this->salesReturns, $this->sales);

        $amount = $this->profitCalculation(
            $totalPurchase,
            $totalSalesArr,
            $totalPurchaseReturns,
            $totalSalesReturn
        );

        return [
            'totalPurchase' => $totalPurchase,
            'totalSalesArr' => $totalSalesArr,
            'totalExpenses' => $this->totalExpenses,
            'amount' => $amount,
            'profit' => $this->profit,
            'purchases' => $this->purchases,
            'sales' => $this->sales,
            'expenses' => $this->expenses,
            'salesReturns' => $this->salesReturns,
            'totalSalesReturn' => $totalSalesReturn,
            'purchaseReturns' => $this->purchaseReturns,
            'totalPurchaseReturns' => $totalPurchaseReturns
        ];
    }

    public function netProfit()
    {
        $this->purchases = Purchase::all();
        $this->sales = Sales::all();
        $this->expenses = Expenses::all();
        $this->totalExpenses = Expenses::sum('amount');
        $this->salesReturns = SalesReturn::all();
        $this->purchaseReturns = PurchaseReturn::all();

        $totalPurchase = $this->totalPurchase->returnTotalPurchase($this->purchases);
        $totalSalesArr = $this->totalSales->returnTotalSales($this->sales);

        $totalPurchaseReturns = $this->totalPurchaseReturns->returnTotalPurchaseReturn($this->purchaseReturns, $this->purchases);
        $totalSalesReturn = $this->totalSalesReturn->returnTotalSalesReturn($this->salesReturns, $this->sales);

        $amount = $this->profitCalculation(
            $totalPurchase,
            $totalSalesArr,
            $totalPurchaseReturns,
            $totalSalesReturn
        );

        return [
            'nettotalPurchase' => $totalPurchase,
            'nettotalSalesArr' => $totalSalesArr,
            'nettotalExpenses' => $this->totalExpenses,
            'netamount' => $amount,
            'netprofit' => $this->profit,
            'netpurchases' => $this->purchases,
            'netsales' => $this->sales,
            'netexpenses' => $this->expenses,
            'netsalesReturns' => $this->salesReturns,
            'nettotalSalesReturn' => $totalSalesReturn,
            'netpurchaseReturns' => $this->purchaseReturns,
            'nettotalPurchaseReturns' => $totalPurchaseReturns
        ];
    }


    public function profitCalculation(
        $totalPurchase,
        $totalSalesArr,
        $totalPurchaseReturns,
        $totalSalesReturn
    ) {
        $amount = $totalSalesArr['sales'] +
            $totalPurchaseReturns -
            ($this->totalExpenses + $totalPurchase + $totalSalesReturn);

        if ($amount > 0) {
            $this->profit = true;
        } else {
            $this->profit = false;
        }
        return $amount;
    }
}