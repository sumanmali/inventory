<?php

namespace App\Services;

class TotalSales
{

    protected $totalSales, $cashTrans, $credit;

    public function returnTotalSales($saleses)
    {
        $this->totalSales = 0;
        $this->cashTrans = 0;
        $this->credit = 0;

        foreach ($saleses as $sales) {
            $this->totalSales += $sales->quantity * $sales->price;
            if ($sales->mode == 0) {
                $this->cashTrans += $sales->quantity * $sales->price;
            } else {
                $this->credit += $sales->quantity * $sales->price;
            }
        }

        return [
            'sales' => $this->totalSales,
            'cash' => $this->cashTrans,
            'credit' => $this->credit
        ];
    }
}