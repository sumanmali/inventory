<?php

namespace App\Services;

class TotalSalesReturn
{

    protected $totalSalesReturn;

    public function __construct()
    {
    }

    public function returnTotalSalesReturn($salesReturns, $sales)
    {
        $this->totalSalesReturn = 0;

        foreach ($salesReturns as $salesReturn) {
            foreach ($sales as $sale) {
                if ($sale->id == $salesReturn->sales_id) {
                    $this->totalSalesReturn += $sale->price * $salesReturn->quantity;
                }
            }
        }
        return $this->totalSalesReturn;
    }
}