<?php

namespace App\Services;

use App\Stock;

class StockMaintainer
{

    protected $stocks;

    public function __construct()
    {
        $this->stocks = Stock::all();
    }

    public function stockMaintainOnPurchase($modelNo, $quantity, $size = NULL)
    {
        $counter = 0;
        foreach ($this->stocks as $stk) {
            if ($stk->model_no === $modelNo) {
                $stock = Stock::where('model_no', $modelNo)->first();
                $stock->stock = $stock->stock + $quantity;
                $stock->update();
                return true;
            } elseif($counter === count($this->stocks)) {
                Stock::create([
                    'model_no' => $modelNo,
                    'stock' => $quantity,
                    'size' => $size
                ]);
                return true;
            }
            else{
                continue;
            }
            $counter++;
        }
        return false;
    }


    public function stockMaintainOnSales($modelNo, $quantity)
    {
        foreach ($this->stocks as $stk) {
            if ($stk->model_no === $modelNo && $stk->stock > 0) {
                $stock = Stock::where('model_no', $modelNo)->first();
                $stock->stock = $stock->stock - $quantity;
                $stock->update();
                return true;
            }
        }
        return false;
    }
}