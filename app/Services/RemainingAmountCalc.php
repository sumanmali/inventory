<?php

namespace App\Services;

use App\Investor;
use App\Expenses;
use App\Purchase;
use App\Sales;

use App\Services\TotalPurchase;
use App\Services\TotalSales;

class RemainingAmountCalc
{

    protected $lot;
    protected $purchases;
    protected $saleses;
    protected $investors;
    protected $expenses;
    protected $totalInvestment;
    protected $totalExpenses;
    protected $remainingAmt;

    protected $totalPurchase;
    protected $totalSales;

    public function __construct(TotalPurchase $totalPurchase, TotalSales $totalSales)
    {
        $this->totalPurchase = $totalPurchase;
        $this->totalSales = $totalSales;
    }

    public function lotWiseCalculator()
    {
        $this->lot = session()->get('lot');
        $this->purchases = Purchase::where('lot_id', $this->lot['id'])->get();
        $this->saleses = Sales::all();
        $this->investors = Investor::where('lot_id', $this->lot['id'])->get();
        $this->expenses = Expenses::where('lot_id', $this->lot['id'])->get();

        $this->totalInvestment = Investor::where('lot_id', $this->lot['id'])->sum('amount');
        $this->totalExpenses = Expenses::where('lot_id', $this->lot['id'])->sum('amount');

        $this->remainingAmt = $this->totalInvestment - $this->totalExpenses -
            $this->totalPurchase->returnTotalPurchase($this->purchases) +
            $this->totalSales->returnTotalSales($this->saleses)['cash'];

        return $this->remainingAmt;
    }
}