<?php

namespace App\Services;

class TotalPurchaseReturn
{

    protected $totalPurchaseReturn;

    public function __construct()
    {
    }

    public function returnTotalPurchaseReturn($purchaseReturns, $purchases)
    {
        $this->totalPurchaseReturn = 0;

        foreach ($purchaseReturns as $purchaseReturn) {
            foreach ($purchases as $purchase) {
                if ($purchase->id == $purchaseReturn->purchase_id) {
                    $this->totalPurchaseReturn += $purchase->price * $purchaseReturn->quantity;
                }
            }
        }
        return $this->totalPurchaseReturn;
    }
}