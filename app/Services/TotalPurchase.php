<?php

namespace App\Services;

class TotalPurchase
{

    protected $totalPurchase, $purchases;

    public function returnTotalPurchase($purchases)
    {
        $this->totalPurchase = 0;
        foreach ($purchases as $purchase) {
            $this->totalPurchase += $purchase->quantity * $purchase->price;
        }
        return $this->totalPurchase;
    }
}