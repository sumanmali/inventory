@extends('layouts.admin')

@section('styles')
<link rel="stylesheet" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
<style>
div#returnModal .form-group {
    display: inline-block;
    margin: 5px 15px;
}
</style>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">
                    Purchase Return Record
                </h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table datatable">
                        <thead class="text-primary">
                            <tr>
                                <th>
                                    S.N
                                </th>
                                <th>
                                    Model No
                                </th>
                                <th>
                                    Quantity
                                </th>
                                <th>
                                    Receipt No.
                                </th>
                                <th>
                                    Customer Details
                                </th>
                                <th>
                                    Size
                                </th>
                                <th>
                                    Brand
                                </th>
                                <th>
                                    Price
                                </th>
                                <th>
                                    Date
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($purchase_returns as $purchase_return)
                            @foreach($purchases as $purchase)
                            @if($purchase->id == $purchase_return->purchase_id)
                            <tr>
                                <td>
                                    {{ $loop->iteration }}
                                </td>
                                <td>
                                    {{ $purchase->model_no }}
                                </td>
                                <td>
                                    {{ $purchase_return->quantity }}
                                </td>
                                <td>
                                    {{ $purchase->receipt_no }}
                                </td>
                                <td>
                                    Company Name: {{ $purchase->company_name }}<br>
                                    PAN: {{ $purchase->PAN }}<br>
                                    Contact Person: {{ $purchase->contact_person }}<br>
                                    Phone: {{ $purchase->contact_person_phone }}
                                </td>
                                <td>
                                    {{ $purchase->price }}
                                </td>
                                <td>
                                    {{ $purchase->size }}
                                </td>
                                <td>
                                    {{ $purchase->brand }}
                                </td>
                                <td>
                                    {{ $purchase_return->return_date }}
                                </td>
                            </tr>
                            @endif
                            @endforeach
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script>
$(document).ready(function() {
    $('.datatable').DataTable();
});
</script>
@endsection