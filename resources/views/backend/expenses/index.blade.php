@extends('layouts.admin')

@section('styles')

@endsection

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<h4 class="card-title">
					Expenses Record
				</h4>
				<div>
					<a href="{{ route('expenses.create') }}" class="btn">
						Add New Expenses
					</a>
				</div>
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<table class="table datatable">
						<thead class="text-primary">                   
							<tr>
								<th>    
									S.N  
								</th>
								<th>
									Title
								</th>
								<th>
									Amount
								</th>
								<th>
									Action
								</th>
							</tr>
						</thead>
						<tbody>
							@foreach($expenses as $expense)
							<tr>
								<td>    
									{{ $loop->iteration }}
								</td>
								<td>
									{{ $expense->title }}
								</td>
								<td>
									{{ $expense->amount }}
								</td>
								<td>
									<a href="{{ route('expenses.edit', $expense->id) }}" class="btn btn-primary btn-block">
										Edit
									</a>
									<form action="{{ route('expenses.destroy', $expense->id) }}" method="POST">
										@csrf
										@method('delete')
										<button class="btn btn-alert btn-block" onclick="promtAlert(event)">Delete</button>
									</form>
								</td>
							</tr>
							@endforeach  
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script>
	function promtAlert(evt){
		var ans = confirm('Are you sure to delete?');
		if(!ans){
			evt.preventDefault();
			evt.stopPropagation();
		}
	}
</script>
@endsection