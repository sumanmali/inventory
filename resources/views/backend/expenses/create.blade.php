@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            @if($canMakeExpenses)
            <div class="card-header">
                <h4 class="card-title">Add New Expenses Record</h4>
            </div>
            <div class="card-body">
                <form action="{{ route('expenses.store') }}" method="POST" class="addForm" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="lot_id" value="{{ $lot->id }}">
                    <div class="form-group col-md-4">
                        <label for="title">Title</label>
                        <input type="text" name="title" class="form-control" required placeholder="Title of Expenses">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="amount">Amount</label>
                        <input type="text" name="amount" class="form-control" required>
                    </div>
                    <button type="submit" class="btn btn-block col-md-4">Submit</button>
                </form>
            </div>
            @else
            <div class="card-header">
                <h4 class="card-title">No Capital</h4>
            </div>
            @endif
        </div>
    </div>
</div>

@endsection