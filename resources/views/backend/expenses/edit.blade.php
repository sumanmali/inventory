@extends('layouts.admin')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<h4 class="card-title">Edit Expenses Record</h4>     
			</div>
			<div class="card-body">
				<form action="{{ route('expenses.update', $expense->id) }}" method="POST" class="addForm" enctype="multipart/form-data">
					@csrf
					@method('PUT')
					<input type="hidden" name="lot_id" value="{{ $lot->id }}">
					<div class="form-group col-md-4">
						<label for="title">Title</label>
						<input type="text" name="title" class="form-control" value="{{ $expense->title}}" required placeholder="Title of Expenses">
					</div>
					<div class="form-group col-md-4">
						<label for="amount">Amount</label>
						<input type="text" name="amount" class="form-control" value="{{ $expense->amount}}" required>
					</div>
					<button type="submit" class="btn btn-block col-md-4">Submit</button>
				</form>
			</div>
		</div>
	</div>
</div>

@endsection