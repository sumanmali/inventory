@extends('layouts.admin')

@section('styles')
<link rel="stylesheet" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
<style>
	div#salesReturn .form-group {
		display: inline-block;
		margin: 5px 15px;
	}
</style>
@endsection

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<h4 class="card-title">
					Sales Return Record
				</h4>
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<table class="table datatable">
						<thead class="text-primary">                   
							<tr>
								<th>    
									S.N  
								</th>
								<th>
									Model No   
								</th>
								<th>
									Quantity
								</th>
								<th>
									Bill No.  
								</th>
								<th>   
									Customer Details
								</th>
								<th>   
									Price  
								</th>
								<th>
									Size
								</th>
								<th>
									Brand
								</th>
								<th>   
									Date  
								</th>
							</tr>
						</thead>
						<tbody>
							@foreach($sales_returns as $sales_record)
							@foreach($sales as $sale)
							@if($sale->id == $sales_record->sales_id)
							<tr>
								<td>    
									{{ $loop->iteration }}
								</td>
								<td> 
									{{ $sale->model_no }}   
								</td>
								<td>
									{{ $sales_record->quantity }}
								</td>
								<td>   
									{{ $sale->bill_no }}  
								</td>
								<td>
									Company Name: {{ $sale->company_name }}<br>
									PAN: {{ $sale->PAN }}<br>
									Contact Person: {{ $sale->customer }}<br>
									Phone: {{ $sale->customer_phone }}
								</td>
								<td>   
									{{ $sale->price }}  
								</td>
								<td>   
									{{ $sale->size }}  
								</td>
								<td>   
									{{ $sale->brand }}  
								</td>
								<td>   
									{{ $sales_record->return_date }}  
								</td>
							</tr>
							@endif
							@endforeach
							@endforeach  
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready( function () {
        $('.datatable').DataTable();
    });
</script>
@endsection