@extends('layouts.admin')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<h4 class="card-title">Edit Investor Record</h4>     
			</div>
			<div class="card-body">
				<form action="{{ route('investor.update', $investor->id) }}" method="POST" class="addForm" enctype="multipart/form-data">
					@csrf
					@method('PUT')
					<input type="hidden" name="lot_id" value="{{ $lot->id }}">
					<div class="form-group col-md-4">
						<label for="name">Name</label>
						<input type="text" name="name" class="form-control" required placeholder="Name of Investor" value="{{ $investor->name }}">
					</div>
					<div class="form-group col-md-4">
						<label for="amount">Amount</label>
						<input type="text" name="amount" class="form-control" required value="{{ $investor->amount }}">
					</div>
					<button type="submit" class="btn btn-block col-md-4">Submit</button>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection