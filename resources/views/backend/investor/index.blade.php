@extends('layouts.admin')

@section('styles')

@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">
                    Investor Record
                </h4>
                <div>
                    <a href="{{ route('investor.create') }}" class="btn">
                        Add New Investor
                    </a>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table datatable">
                        <thead class="text-primary">
                            <tr>
                                <th>
                                    S.N
                                </th>
                                <th>
                                    Name
                                </th>
                                <th>
                                    Amount
                                </th>
                                <th>
                                    Action
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($investors as $investor)
                            <tr>
                                <td>
                                    {{ $loop->iteration }}
                                </td>
                                <td>
                                    {{ $investor->name }}
                                </td>
                                <td>
                                    {{ $investor->amount }}
                                </td>
                                <td>
                                    <a href="{{ route('investor.edit', $investor->id) }}"
                                        class="btn btn-primary mb-1 btn-block">
                                        Edit
                                    </a>
                                    <form action="{{ route('investor.destroy', $investor->id) }}" method="POST">
                                        @csrf
                                        @method('delete')
                                        <button class="btn btn-alert btn-block"
                                            onclick="promtAlert(event)">Delete</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
function promtAlert(evt) {
    var ans = confirm('Are you sure to delete?');
    if (!ans) {
        evt.preventDefault();
        evt.stopPropagation();
    }
}
</script>
@endsection