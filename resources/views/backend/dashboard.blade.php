@extends('layouts.admin')

@section('styles')
<link rel="stylesheet" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
<style>
.stock {
    border: 1px solid #efefef;
    text-align: center;
    padding: 9px 10px;
    box-shadow: 3px 3px 8px #c2c2c2c7;
    display: inline-block;
    background: #2ca8ff;
    color: white;
}
</style>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-4">
        <div class="card card-chart">
            <div class="card-header collapsible" data-toggle="collapse" data-target="#investment">
                <h5 class="text-center">Total Investments: <br><strong>{{ $totalInvestment }}</strong></h5>
            </div>
            <div class="card-body collapse collapse-panel" id="investment">
                @foreach($investors as $investment)
                <div class="row">
                    <div class="col-sm-6">
                        {{ $investment->name }}
                    </div>
                    <div class="col-sm-6">
                        {{ $investment->amount }}
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card card-chart">
            <div class="card-header collapsible" data-toggle="collapse" data-target="#expenseAmt">
                <h5 class="text-center">Total Expenses: <br><strong>{{ $totalExpenses }}</strong></h5>
            </div>
            <div class="card-body collapse collapse-panel" id="expenseAmt">
                @foreach($expenses as $expense)
                <div class="row">
                    <div class="col-sm-6">
                        {{ $expense->title }}
                    </div>
                    <div class="col-sm-6">
                        {{ $expense->amount }}
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card card-chart">
            <div class="card-header collapsible" data-toggle="collapse" data-target="#remainingAmt">
                <h5 class="text-center">Remaining Amount: <br><strong>{{ $remainingAmt }}</strong></h5>
            </div>
            <div class="card-body collapse collapse-panel" id="remainingAmt">
                <div class="row">
                    <div class="col-sm-8">
                        Cash in Hand
                    </div>
                    <div class="col-sm-4">
                        {{ $remainingAmt }}
                    </div>
                    <div class="col-sm-8">
                        Sundry Creditor
                    </div>
                    <div class="col-sm-4">
                        {{ $totalSalesArr['credit'] }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="card card-chart">
            <div class="card-header collapsible" data-toggle="collapse" data-target="#salesAmt">
                <h5 class="text-center">Total Sales: <br><strong>{{ $totalSalesArr['sales'] }}</strong></h5>
            </div>
            <div class="card-body collapse collapse-panel" id="salesAmt">
                <div class="row">
                    <div class="col-sm-6">
                        Cash
                    </div>
                    <div class="col-sm-6">
                        {{ $totalSalesArr['cash'] }}
                    </div>
                    <div class="col-sm-6">
                        Credit
                    </div>
                    <div class="col-sm-6">
                        {{ $totalSalesArr['credit'] }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="card card-chart">
            <div class="card-header collapsible" data-toggle="collapse" data-target="#totalPurchase">
                <h5 class="text-center">Total Purchase: <br><strong>{{ $totalPurchase }}</strong></h5>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
        <div class="card card-chart">
            <div class="card-header">
                <h5 class="card-category">( {{ $saleses->count() }} ) <em>Sales</em></h5>
                <h4 class="card-title">All Sales</h4>
                <div class="dropdown">
                    <button type="button"
                        class="btn btn-round btn-outline-default dropdown-toggle btn-simple btn-icon no-caret"
                        data-toggle="dropdown">
                        <i class="now-ui-icons ui-1_simple-add"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="{{ route('sales.index') }}">Add Sales</a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="chart-area">
                    <table class="datatable table">
                        <thead>
                            <th>S.N</th>
                            <th>Model No</th>
                            <th>Quantity</th>
                        </thead>
                        <tbody>
                            @foreach($saleses as $sales)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $sales->model_no }}</td>
                                <td>{{ $sales->quantity }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="now-ui-icons arrows-1_refresh-69"></i> Just Updated
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="card card-chart">
            <div class="card-header">
                <h5 class="card-category">( {{ $purchases->count() }} ) <em>Purchase</em></h5>
                <h4 class="card-title">All Purchases</h4>
                <div class="dropdown">
                    <button type="button"
                        class="btn btn-round btn-outline-default dropdown-toggle btn-simple btn-icon no-caret"
                        data-toggle="dropdown">
                        <i class="now-ui-icons ui-1_simple-add"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="{{ route('purchase.create') }}">Add Purchase</a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="chart-area">
                    <table class="datatable table">
                        <thead>
                            <th>S.N</th>
                            <th>Model No</th>
                            <th>Quantity</th>
                        </thead>
                        <tbody>
                            @foreach($purchases as $purchase)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $purchase->model_no }}</td>
                                <td>{{ $purchase->quantity }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card-footer">
                <div class="stats">
                    Just Updated
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script>
$(document).ready(function() {
    $('.datatable').DataTable();
});
</script>
@endsection