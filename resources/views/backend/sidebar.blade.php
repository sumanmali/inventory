<div class="sidebar" data-color="blue">
    <div class="logo">
        <a href="/" class="simple-text logo-normal">
            Inventory Management
        </a>
    </div>
    <div class="sidebar-wrapper" id="sidebar-wrapper">
        <ul class="nav">
            <li class="active ">
                <a href="{{ route('lot.index') }}">
                    <i class="now-ui-icons design_app"></i>
                    <p>Dashboard</p>
                </a>
            </li>
            <li>
                <a href="{{ route('investor.index') }}">

                    <i class="now-ui-icons users_single-02"></i>

                    <p>Investors</p>
                </a>
            </li>
            <li>
                <a href="{{ route('expenses.index') }}">

                    <i class="now-ui-icons business_money-coins"></i>

                    <p>Expenses</p>
                </a>
            </li>
            <li>
                <a href="{{ route('purchase.index') }}">

                    <i class="now-ui-icons shopping_bag-16"></i>

                    <p>Purchase</p>
                </a>
            </li>
            <li>
                <a href="{{ route('sales.index') }}">

                    <i class="now-ui-icons arrows-1_share-66"></i>

                    <p>Sales</p>
                </a>
            </li>
            <li>
                <a href="{{ route('purchaseReturn.index') }}">

                    <i class="now-ui-icons arrows-1_cloud-upload-94"></i>

                    <p>Purchase Return</p>
                </a>
            </li>
            <li>
                <a href="{{ route('salesReturn.index') }}">

                    <i class="now-ui-icons arrows-1_cloud-download-93"></i>

                    <p>Sales Return</p>
                </a>
            </li>
            <li>
                <a href="/profitLoss">

                    <i class="now-ui-icons business_money-coins"></i>

                    <p>Profit / Loss</p>
                </a>
            </li>
            <li>
                <a href="/stocks">

                    <i class="now-ui-icons business_briefcase-24"></i>

                    <p>Stocks</p>
                </a>
            </li>
        </ul>
    </div>
</div>