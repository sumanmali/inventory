@extends('layouts.admin')

@section('styles')
<style>
.card-body.profit-loss .table {
    border: 1px solid #cecece;
}

.profit-loss .table .border {
    border: 1px solid #cecece;
}

.collapse-panel td {
    text-align: center;
    background: #efefef;
    border-bottom: 1px solid white;
}

.red {
    background-color: red;
    color: white;
    padding: 4px 8px;
    box-shadow: 3px 5px 4px #cecece;
}

.green {
    color: green;
}
</style>
@endsection

@section('content')
<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">
                    LotWise Profit / Loss
                </h4>
            </div>
            <div class="card-body profit-loss">
                <div class="table-responsive">
                    <table class="table table-hover table-borderless">
                        <thead class="border">
                            <th>Particulars</th>
                            <th>Amount</th>
                        </thead>
                        <tbody>

                            <tr data-toggle="collapse" data-target="#netexpenses">
                                <td>
                                    Total Expenses
                                </td>
                                <td>
                                    {{ $lotProfit['totalExpenses'] }}
                                </td>
                            </tr>
                            @foreach($lotProfit['expenses'] as $expense)
                            <tr id="netexpenses" class="collapse collapse-panel">
                                <td>
                                    {{ $expense->title }}
                                </td>
                                <td>
                                    {{ $expense->amount }}
                                </td>
                            </tr>
                            @endforeach
                            <tr data-toggle="collapse" data-target="#netpurchase">
                                <td>
                                    Total Purchases
                                </td>
                                <td>
                                    {{ $lotProfit['totalPurchase'] }}
                                </td>
                            </tr>
                            @foreach($lotProfit['purchases'] as $purchase)
                            <tr id="netpurchase" class="collapse collapse-panel">
                                <td>
                                    {{ $purchase->model_no }} ( Quantity: {{ $purchase->quantity }} ) ( Price:
                                    {{ $purchase->price }} )
                                </td>
                                <td>
                                    {{ $purchase->price * $purchase->quantity }}
                                </td>
                            </tr>
                            @endforeach
                            <tr data-toggle="collapse" data-target="#netpurchaseReturns">
                                <td>
                                    Total purchase Return
                                </td>
                                <td>
                                    {{ $lotProfit['totalPurchaseReturns'] }}
                                </td>
                            </tr>
                            @foreach($lotProfit['purchaseReturns'] as $purchaseReturn)
                            @foreach($lotProfit['purchases'] as $purchase)
                            @if($purchase->id == $salesReturn->purchase_id)
                            <tr id="netpurchaseReturns" class="collapse collapse-panel">
                                <td>
                                    {{ $purchase->model_no }} ( Quantity: {{ $purchaseReturn->quantity }} ) ( Price:
                                    {{ $purchase->price }} )
                                </td>
                                <td>
                                    {{ $purchase->price * $purchaseReturn->quantity }}
                                </td>
                            </tr>
                            @endif
                            @endforeach
                            @endforeach
                            <tr data-toggle="collapse" data-target="#netsales">
                                <td>
                                    Total Sales
                                </td>
                                <td>
                                    {{ $lotProfit['totalSalesArr']['sales'] }} &nbsp; &nbsp;<span
                                        class="{{ $lotProfit['totalSalesArr']['credit'] ? 'red' : ''}}">
                                        Credit:
                                        {{ $lotProfit['totalSalesArr']['credit'] }}</span>
                                </td>
                            </tr>
                            @foreach($lotProfit['sales'] as $sale)
                            <tr id="netsales" class="collapse collapse-panel">
                                <td>
                                    {{ $sale->model_no }} ( Quantity: {{ $sale->quantity }} ) ( Price:
                                    {{ $sale->price }} )
                                </td>
                                <td>
                                    {{ $sale->price * $sale->quantity }}
                                    @if($sale->mode == 1)
                                    <span class="red">Credit</span>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                            <tr data-toggle="collapse" data-target="#netsalesReturns">
                                <td>
                                    Total Sales Return
                                </td>
                                <td>
                                    {{ $lotProfit['totalSalesReturn'] }}
                                </td>
                            </tr>
                            @foreach($lotProfit['salesReturns'] as $salesReturn)
                            @foreach($lotProfit['sales'] as $sale)
                            @if($sale->id == $salesReturn->sales_id)
                            <tr id="netsalesReturns" class="collapse collapse-panel">
                                <td>
                                    {{ $sale->model_no }} ( Quantity: {{ $salesReturn->quantity }} ) ( Price:
                                    {{ $sale->price }} )
                                </td>
                                <td>
                                    {{ $sale->price * $salesReturn->quantity }}
                                </td>
                            </tr>
                            @endif
                            @endforeach
                            @endforeach
                            <tr class="border">
                                <td>
                                    {{ $lotProfit['profit'] ? 'Profit Amount' : 'Loss Amount' }}
                                </td>
                                <td style="color: {{ $lotProfit['profit']? 'green' : 'red' }}">
                                    {{ $lotProfit['profit'] ? $lotProfit['amount'] : -$lotProfit['amount'] }}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">
                    Net Profit / Loss
                </h4>
            </div>
            <div class="card-body profit-loss">
                <div class="table-responsive">
                    <table class="table table-hover table-borderless">
                        <thead class="border">
                            <th>Particulars</th>
                            <th>Amount</th>
                        </thead>
                        <tbody>

                            <tr data-toggle="collapse" data-target="#expenses">
                                <td>
                                    Total Expenses
                                </td>
                                <td>
                                    {{ $netProfit['nettotalExpenses'] }}
                                </td>
                            </tr>
                            @foreach($netProfit['netexpenses'] as $expense)
                            <tr id="expenses" class="collapse collapse-panel">
                                <td>
                                    {{ $expense->title }}
                                </td>
                                <td>
                                    {{ $expense->amount }}
                                </td>
                            </tr>
                            @endforeach
                            <tr data-toggle="collapse" data-target="#purchase">
                                <td>
                                    Total Purchases
                                </td>
                                <td>
                                    {{ $netProfit['nettotalPurchase'] }}
                                </td>
                            </tr>
                            @foreach($netProfit['netpurchases'] as $purchase)
                            <tr id="purchase" class="collapse collapse-panel">
                                <td>
                                    {{ $purchase->model_no }} ( Quantity: {{ $purchase->quantity }} ) ( Price:
                                    {{ $purchase->price }} )
                                </td>
                                <td>
                                    {{ $purchase->price * $purchase->quantity }}
                                </td>
                            </tr>
                            @endforeach
                            <tr data-toggle="collapse" data-target="#purchaseReturns">
                                <td>
                                    Total purchase Return
                                </td>
                                <td>
                                    {{ $netProfit['nettotalPurchaseReturns'] }}
                                </td>
                            </tr>
                            @foreach($netProfit['netpurchaseReturns'] as $purchaseReturn)
                            @foreach($netProfit['netpurchases'] as $purchase)
                            @if($purchase->id == $salesReturn->purchase_id)
                            <tr id="purchaseReturns" class="collapse collapse-panel">
                                <td>
                                    {{ $purchase->model_no }} ( Quantity: {{ $purchaseReturn->quantity }} ) ( Price:
                                    {{ $purchase->price }} )
                                </td>
                                <td>
                                    {{ $purchase->price * $purchaseReturn->quantity }}
                                </td>
                            </tr>
                            @endif
                            @endforeach
                            @endforeach
                            <tr data-toggle="collapse" data-target="#sales">
                                <td>
                                    Total Sales
                                </td>
                                <td>
                                    {{ $netProfit['nettotalSalesArr']['sales'] }} &nbsp; &nbsp;<span
                                        class="{{ $netProfit['nettotalSalesArr']['credit'] ? 'red' : ''}}">
                                        Credit:
                                        {{ $netProfit['nettotalSalesArr']['credit'] }}</span>
                                </td>
                            </tr>
                            @foreach($netProfit['netsales'] as $sale)
                            <tr id="sales" class="collapse collapse-panel">
                                <td>
                                    {{ $sale->model_no }} ( Quantity: {{ $sale->quantity }} ) ( Price:
                                    {{ $sale->price }} )
                                </td>
                                <td>
                                    {{ $sale->price * $sale->quantity }}
                                    @if($sale->mode == 1)
                                    <span class="red">Credit</span>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                            <tr data-toggle="collapse" data-target="#salesReturns">
                                <td>
                                    Total Sales Return
                                </td>
                                <td>
                                    {{ $netProfit['nettotalSalesReturn'] }}
                                </td>
                            </tr>
                            @foreach($netProfit['netsalesReturns'] as $salesReturn)
                            @foreach($netProfit['netsales'] as $sale)
                            @if($sale->id == $salesReturn->sales_id)
                            <tr id="salesReturns" class="collapse collapse-panel">
                                <td>
                                    {{ $sale->model_no }} ( Quantity: {{ $salesReturn->quantity }} ) ( Price:
                                    {{ $sale->price }} )
                                </td>
                                <td>
                                    {{ $sale->price * $salesReturn->quantity }}
                                </td>
                            </tr>
                            @endif
                            @endforeach
                            @endforeach
                            <tr class="border">
                                <td>
                                    {{ $netProfit['netprofit'] ? 'Profit Amount' : 'Loss Amount' }}
                                </td>
                                <td style="color: {{ $netProfit['netprofit']? 'green' : 'red' }}">
                                    {{ $netProfit['netprofit'] ? $netProfit['netamount'] : -$netProfit['netamount'] }}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection