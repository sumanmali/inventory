@extends('layouts.admin')

@section('styles')
<link rel="stylesheet" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
<style>
div#returnModal .form-group {
    display: inline-block;
    margin: 5px 15px;
}

.bred {
    background-color: red !important;
    color: white;
}
</style>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">
                    Stock Status
                </h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table datatable">
                        <thead class="text-primary">
                            <tr>
                                <th>
                                    S.N
                                </th>
                                <th>
                                    Model No
                                </th>
                                <th>
                                    Size
                                </th>
                                <th>
                                    Stock
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($overall_stocks as $stock)
                            <tr class="{{ $stock->stock <= 0 ? 'bred' : ''}}">
                                <td>
                                    {{ $loop->iteration }}
                                </td>
                                <td>
                                    {{ $stock->model_no }}
                                </td>
                                <td>
                                    {{ $stock->size }}
                                </td>
                                <td>
                                    {{ $stock->stock }}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script>
$(document).ready(function() {
    $('.datatable').DataTable();
});
</script>
@endsection