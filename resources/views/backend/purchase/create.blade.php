@extends('layouts.admin')

@section('styles')
<style>
    #addVendor form {
        width: 100%;
    }
</style>
@endsection

@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            @if($canPurchase)
            <div class="card-header">
                <h4 class="card-title">Add New Purchase Record</h4>
            </div>
            <div class="card-body">
                <form action="{{ route('purchase.store') }}" method="POST" class="addForm" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="lot_id" value="{{ $lot->id }}">
                    <div class="form-group col-md-4">
                        <label for="model_no">Model Number</label>
                        <input type="text" name="model_no" class="form-control" required placeholder="Product id">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="quantity">Quantity</label>
                        <input type="text" name="quantity" class="form-control" required>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="receipt_no">Receipt Number</label>
                        <input type="text" name="receipt_no" class="form-control">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="price">Price</label>
                        <input type="text" name="price" class="form-control" required>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="brand">Brand Name</label>
                        <input type="text" name="brand" class="form-control">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="size">Size</label>
                        <input type="text" name="size" class="form-control">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="vendor_id">Vendor</label>
                        <select name="vendor_id" id="vendorSelect" class="form-control">
                            <option value="">Select the Vendor</option>
                            @foreach($vendors as $vendor)
                            <option value="{{ $vendor->id }}">{{ $vendor->contact_person }}</option>
                            @endforeach
                        </select>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addVendor">
                            Didn't find Vendor?
                        </button>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="purchase_date">Purchase Date</label>
                        <input type="date" name="purchase_date" class="form-control" required>
                    </div>
                    <button type="submit" class="btn btn-block col-md-4">Submit</button>
                </form>
            </div>
            @else
            <div class="card-header">
                <h4 class="card-title">No Capital Left</h4>
            </div>
            @endif
        </div>
    </div>
</div>

<div class="modal fade" id="addVendor" tabindex="-1" role="dialog" aria-labelledby="addVendor" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form>
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="addVendor">Add a New Vendor</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="lot_id" value="{{ $lot->id }}">
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label for="company_name">Company Name</label>
                        </div>

                        <div class="col-sm-8">
                            <input type="text" name="company_name" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label for="PAN">PAN</label>
                        </div>

                        <div class="col-sm-8">
                            <input type="text" name="PAN" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label for="contact_person">Contact Person Name</label>
                        </div>

                        <div class="col-sm-8">
                            <input type="text" name="contact_person" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label for="location">Location</label>
                        </div>

                        <div class="col-sm-8">
                            <input type="text" name="location" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label for="contact_person_phone">Contact Person Phone</label>
                        </div>

                        <div class="col-sm-8">
                            <input type="text" name="contact_person_phone" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-submit">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $.ajaxSetup({

        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });



    $(".btn-submit").click(function(e) {
        e.preventDefault();
        var company_name = $("input[name=company_name]").val();
        var lot_id = $("input[name=lot_id]").val();
        var PAN = $("input[name=PAN]").val();
        var contact_person = $("input[name=contact_person]").val();
        var location = $("input[name=location]").val();
        var contact_person_phone = $("input[name=contact_person_phone]").val();
        $.ajax({
            type: 'POST',
            url: '/vendors/store',
            data: {
                company_name: company_name,
                lot_id: lot_id,
                PAN: PAN,
                contact_person: contact_person,
                location: location,
                contact_person_phone: contact_person_phone
            },
            success: function(data) {
                $('#addVendor').modal('hide');
                $('#vendorSelect').append(new Option(data[1]['contact_person'], data[1]['id']));
            },
            error: function(data, textStatus, errorThrown) {
                console.log(data);
            }
        });
    });
</script>

@endsection