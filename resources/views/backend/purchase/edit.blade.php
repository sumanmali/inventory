@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Update purchase Record</h4>
            </div>
            <div class="card-body">
                <form action="{{ route('purchase.update', $purchase->id ) }}" method="POST" class="addForm" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <input type="hidden" name="lot_id" value="{{ $lot->id }}">
                    <div class="form-group col-md-4">
                        <label for="model_no">Model Number</label>
                        <input type="text" name="model_no" class="form-control" required placeholder="Product id" value="{{ $purchase->model_no }}">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="quantity">Quantity</label>
                        <input type="text" name="quantity" class="form-control" required value="{{ $purchase->quantity }}">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="receipt_no">Receipt Number</label>
                        <input type="text" name="receipt_no" class="form-control" value="{{ $purchase->receipt_no }}">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="price">Price</label>
                        <input type="text" name="price" class="form-control" required value="{{ $purchase->price }}">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="brand">Brand Name</label>
                        <input type="text" name="brand" class="form-control" value="{{ $purchase->brand }}">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="size">Size</label>
                        <input type="text" name="size" class="form-control" value="{{ $purchase->size }}">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="vendor_id">Vendor</label>
                        <select name="vendor_id" id="vendorSelect" class="form-control">
                            @foreach($vendors as $vendor)
                            @if($vendor->id === $purchase->vendor_id)
                            <option value="{{ $vendor->id }}" selected>{{ $vendor->contact_person }}</option>
                            @else
                            <option value="{{ $vendor->id }}">{{ $vendor->contact_person }}</option>
                            @endif
                            @endforeach
                        </select>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addVendor">
                            Didn't find Vendor?
                        </button>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="purchase_date">purchase Date</label>
                        <input type="date" name="purchase_date" class="form-control" required value="{{ $purchase->purchase_date }}">
                    </div>
                    <button type="submit" class="btn btn-block col-md-4">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="addVendor" tabindex="-1" role="dialog" aria-labelledby="addVendor" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form>
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="addVendor">Add a New Vendor</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="lot_id" value="{{ $lot->id }}">
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label for="company_name">Company Name</label>
                        </div>

                        <div class="col-sm-8">
                            <input type="text" name="company_name" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label for="PAN">PAN</label>
                        </div>

                        <div class="col-sm-8">
                            <input type="text" name="PAN" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label for="contact_person">Contact Person Name</label>
                        </div>

                        <div class="col-sm-8">
                            <input type="text" name="contact_person" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label for="location">Location</label>
                        </div>

                        <div class="col-sm-8">
                            <input type="text" name="location" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label for="contact_person_phone">Contact Person Phone</label>
                        </div>

                        <div class="col-sm-8">
                            <input type="text" name="contact_person_phone" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-submit">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $.ajaxSetup({

        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(".btn-submit").click(function(e) {
        e.preventDefault();
        var company_name = $("input[name=company_name]").val();
        var lot_id = $("input[name=lot_id]").val();
        var PAN = $("input[name=PAN]").val();
        var contact_person = $("input[name=contact_person]").val();
        var location = $("input[name=location]").val();
        var contact_person_phone = $("input[name=contact_person_phone]").val();
        $.ajax({
            type: 'POST',
            url: '/vendors/store',
            data: {
                company_name: company_name,
                lot_id: lot_id,
                PAN: PAN,
                contact_person: contact_person,
                location: location,
                contact_person_phone: contact_person_phone
            },
            success: function(data) {
                $('#addVendor').modal('hide');
                $('#vendorSelect').append(new Option(data[1]['contact_person'], data[1]['id']));
            },
            error: function(data, textStatus, errorThrown) {
                console.log(data);
            }
        });
    });
</script>
@endsection