@extends('layouts.admin')

@section('styles')
<link rel="stylesheet" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
<style>
div#returnModal .form-group {
    display: inline-block;
    margin: 5px 15px;
}
</style>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">
                    Purchase Record
                </h4>
                <div>
                    <a href="{{ route('purchase.create') }}" class="btn">
                        Add New Purchase
                    </a>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table datatable">
                        <thead class="text-primary">
                            <tr>
                                <th>
                                    S.N
                                </th>
                                <th>
                                    Model No
                                </th>
                                <th>
                                    Quantity
                                </th>
                                <th>
                                    Bill No.
                                </th>
                                <th>
                                    Customer Details
                                </th>
                                <th>
                                    Price
                                </th>
                                <th>
                                    Size
                                </th>
                                <th>
                                    Brand
                                </th>
                                <th>
                                    Purchase Date
                                </th>
                                <th>
                                    Action
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($purchases as $purchase)
                            <tr>
                                <td>
                                    {{ $loop->iteration }}
                                </td>
                                <td>
                                    {{ $purchase->model_no }}
                                </td>
                                <td>
                                    {{ $purchase->quantity }}
                                </td>
                                <td>
                                    {{ $purchase->receipt_no }}
                                </td>
                                <td>
                                    @foreach($vendors as $vendor)
                                    @if($purchase->vendor_id == $vendor->id)
                                    Company Name: {{ $vendor->company_name }}<br>
                                    PAN: {{ $vendor->PAN }}<br>
                                    Contact Person: {{ $vendor->contact_person }}<br>
                                    Phone: {{ $vendor->contact_person_phone }}<br>
                                    Location: {{ $vendor->location }}
                                    @endif
                                    @endforeach
                                </td>
                                <td>
                                    {{ $purchase->price }}
                                </td>
                                <td>
                                    {{ $purchase->size }}
                                </td>
                                <td>
                                    {{ $purchase->brand }}
                                </td>
                                <td>
                                    {{ $purchase->purchase_date }}
                                </td>
                                <td>
                                    <a href="{{ route('purchase.edit', $purchase->id) }}"
                                        class="btn btn-success btn-block mb-1">Edit</a>

                                    <button type="button" class="btn btn-primary btn-block mb-1" data-toggle="modal"
                                        data-target="#returnModal">
                                        Return ?
                                    </button>

                                    <div class="modal fade" id="returnModal" tabindex="-1" role="dialog"
                                        aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <form action="{{ route('purchaseReturn.store') }}" method="POST"
                                                enctype="multipart/form-data">
                                                @csrf
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="title">Purchase Return</h5>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <input type="hidden" name="lot_id" value="{{ $lot->id }}">
                                                        <input type="hidden" name="purchase_id"
                                                            value="{{ $purchase->id }}">
                                                        <div class="form-group">
                                                            <label for="quantity">Quantity</label>
                                                            <input type="text" name="quantity" required
                                                                class="form-control">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="return_date">Return Date</label>
                                                            <input type="date" name="return_date" required
                                                                class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary"
                                                            data-dismiss="modal">Close</button>
                                                        <button type="submit" class="btn btn-primary">Save
                                                            changes</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>

                                    <form action="{{ route('purchase.destroy', $purchase->id) }}" method="POST">
                                        @csrf
                                        @method('delete')
                                        <button class="btn btn-danger btn-block"
                                            onclick="promtAlert(event)">Delete</button>
                                    </form>

                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection



@section('scripts')
<script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script>
function promtAlert(evt) {
    var ans = confirm('Are you sure to delete?');
    if (!ans) {
        evt.preventDefault();
        evt.stopPropagation();
    }
}
$(document).ready(function() {
    $('.datatable').DataTable();
});
</script>
@endsection