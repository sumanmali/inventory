@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            @if(count($stocks))
            <div class="card-header">
                <h4 class="card-title">Editing Sales Record</h4>
            </div>
            <div class="card-body">
                <form action="{{ route('sales.update', $sale->id) }}" method="POST" class="addForm"
                    enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="form-group col-md-4">
                        <label for="model_no">Model Number</label>
                        <select name="model_no" id="modelNo" class="form-control"
                            onchange="limitQuantity(<?php echo $stocks ?>)">
                            @foreach($stocks as $stock)
                            @if($stock->model_no === $sale->model_no)
                            <option value="{{ $stock->model_no }}" selected>{{ $stock->model_no }}</option>
                            @else
                            <option value="{{ $stock->model_no }}">{{ $stock->model_no }}</option>
                            @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="quantity">Quantity</label>
                        <input type="number" name="quantity" class="form-control" value="{{ $sale->quantity }}"
                            id="quantity" min="1">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="bill_no">Bill Number</label>
                        <input type="text" name="bill_no" class="form-control" value="{{ $sale->bill_no }}">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="price">Price</label>
                        <input type="text" name="price" class="form-control" value="{{ $sale->price }}">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="mode">Mode of Payment</label>
                        <select name="mode" id="" class="form-control">
                            @if($sale->mode == 1)
                            <option value="1" selected>Credit</option>
                            <option value="0">Cash</option>
                            @else
                            <option value="0" selected>Cash</option>
                            <option value="1">Credit</option>
                            @endif
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="sales_date">Sales Date</label>
                        <input type="date" name="sales_date" class="form-control" value="{{ $sale->sales_date }}">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="customer_id">Customer</label>
                        <select name="customer_id" id="customerSelect" class="form-control">
                            @foreach($customers as $customer)
                            @if($customer->id === $sale->customer_id)
                            <option value="{{ $customer->id }}" selected>{{ $customer->customer }}</option>
                            @else
                            <option value="">Select the Customer</option>
                            @endif
                            @endforeach
                        </select>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addCustomer">
                            Didn't find Customer?
                        </button>
                    </div>

                    <button type="submit" class="btn btn-block col-md-4">Submit</button>
                </form>
            </div>
            @else
            <div class="card-header">
                <h4 class="card-title">Out of Stock, Please enter a purchase first</h4>
            </div>
            @endif
        </div>
    </div>
</div>

<div class="modal fade" id="addCustomer" tabindex="-1" role="dialog" aria-labelledby="addCustomerTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form>
                <div class="modal-header">
                    <h5 class="modal-title" id="addCustomerTitle">Add a New Customer</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    @csrf
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label for="company_name">Company Name</label>
                        </div>

                        <div class="col-sm-8">
                            <input type="text" name="company_name" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label for="PAN">PAN</label>
                        </div>

                        <div class="col-sm-8">
                            <input type="text" name="PAN" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label for="customer">Customer Name</label>
                        </div>

                        <div class="col-sm-8">
                            <input type="text" name="customer" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label for="location">Location</label>
                        </div>

                        <div class="col-sm-8">
                            <input type="text" name="location" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label for="customer_phone">Customer Phone</label>
                        </div>

                        <div class="col-sm-8">
                            <input type="text" name="customer_phone" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label for="points">Purchase Points</label>
                        </div>

                        <div class="col-sm-8">
                            <input type="text" name="points" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-submitcustomer">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
function limitQuantity(stocks) {
    var modelNo = $('#modelNo').val();
    stocks.forEach((stock) => {
        if (stock.model_no == modelNo) {
            if (stock.stock > 0) {
                $('#quantity').attr('max', stock.stock);
            } else {
                window.location.href = '/sales';
            }
        }
    });
}

$.ajaxSetup({

    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});



$(".btn-submitcustomer").click(function(e) {
    e.preventDefault();
    var company_name = $("input[name=company_name]").val();
    var PAN = $("input[name=PAN]").val();
    var customer = $("input[name=customer]").val();
    var location = $("input[name=location]").val();
    var customer_phone = $("input[name=customer_phone]").val();
    var points = $("input[name=points]").val();
    $.ajax({
        type: 'POST',
        url: '/customers/store',
        data: {
            company_name: company_name,
            PAN: PAN,
            customer: customer,
            location: location,
            customer_phone: customer_phone,
            points: points
        },
        success: function(data) {
            $('#addCustomer').modal('hide');
            $('#customerSelect').append(new Option(data[1]['customer'], data[1]['id']));
            console.log(data);
        },
        error: function(data, textStatus, errorThrown) {
            console.log(data);
        }
    });
});
</script>

@endsection