@extends('layouts.admin')

@section('styles')
<link rel="stylesheet" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
<style>
div#salesReturn .form-group {
    display: inline-block;
    margin: 5px 15px;
}

.red {
    background-color: red !important;
    color: white;
}

.red:after {
    content: 'Credit';
    position: absolute;
    left: -13px;
    color: black;
    background: white;
    padding: 3px 35px;
    transform: rotate(-36deg);
    margin-top: 10px;
}
</style>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">
                    Sales Record
                </h4>
                <div>
                    <a href="{{ route('sales.create') }}" class="btn">
                        Add New Sales
                    </a>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table datatable">
                        <thead class="text-primary">
                            <tr>
                                <th>
                                    S.N
                                </th>
                                <th>
                                    Model No
                                </th>
                                <th>
                                    Quantity
                                </th>
                                <th>
                                    Bill No.
                                </th>
                                <th>
                                    Customer Details
                                </th>
                                <th>
                                    Price
                                </th>
                                <th>
                                    Quantity
                                </th>
                                <th>
                                    Date
                                </th>
                                <th>
                                    Action
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($sales as $sales_record)
                            <tr class="{{ $sales_record->mode == 1 ? 'red' : '' }}">
                                <td>
                                    {{ $loop->iteration }}
                                </td>
                                <td>
                                    {{ $sales_record->model_no }}
                                </td>
                                <td>
                                    {{ $sales_record->quantity }}
                                </td>
                                <td>
                                    {{ $sales_record->bill_no }}
                                </td>
                                <td>
                                    @foreach($customers as $customer)
                                    @if($customer->id == $sales_record->customer_id)
                                    Company Name: {{ $customer->company_name }}<br>
                                    PAN: {{ $customer->PAN }}<br>
                                    Contact Person: {{ $customer->customer }}<br>
                                    Phone: {{ $customer->customer_phone }}<br>
                                    Location: {{ $customer->location }}
                                    @endif
                                    @endforeach
                                </td>
                                <td>
                                    {{ $sales_record->price }}
                                </td>
                                <td>
                                    {{ $sales_record->quantity }}
                                </td>
                                <td>
                                    {{ $sales_record->sales_date }}
                                </td>
                                <td>
                                    <a href="{{ route('sales.edit', $sales_record->id) }}"
                                        class="btn btn-success btn-block mb-1">Edit</a>

                                    <button type="button" class="btn btn-primary btn-block mb-1" data-toggle="modal"
                                        data-target="#salesReturn">
                                        Return ?
                                    </button>

                                    <div class="modal fade" id="salesReturn" tabindex="-1" role="dialog"
                                        aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <form action="{{ route('salesReturn.store') }}" method="POST"
                                                enctype="multipart/form-data">
                                                @csrf
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="title">Sales Return</h5>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <input type="hidden" name="lot_id" value="{{ $lot->id }}">
                                                        <input type="hidden" name="sales_id"
                                                            value="{{ $sales_record->id }}">
                                                        <div class="form-group">
                                                            <label for="quantity">Quantity</label>
                                                            <input type="text" name="quantity" required
                                                                class="form-control">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="return_date">Return Date</label>
                                                            <input type="date" name="return_date" required
                                                                class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary"
                                                            data-dismiss="modal">Close</button>
                                                        <button type="submit" class="btn btn-primary">Save
                                                            changes</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>

                                    <form action="{{ route('sales.destroy', $sales_record->id) }}" method="POST">
                                        @csrf
                                        @method('delete')
                                        <button class="btn btn-danger btn-block"
                                            onclick="promtAlert(event)">Delete</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script>
function promtAlert(evt) {
    var ans = confirm('Are you sure to delete?');
    if (!ans) {
        evt.preventDefault();
        evt.stopPropagation();
    }
}
$(document).ready(function() {
    $('.datatable').DataTable();
});
</script>
@endsection