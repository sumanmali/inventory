<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Inventory Management</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" />
    <!--   Core JS Files   -->
    <script src="{{ asset('assets/js/core/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/js/core/popper.min.js') }}"></script>
    <script src="{{ asset('assets/js/core/bootstrap.min.js') }}"></script>
    <!-- Styles -->
    <style>
    html,
    body {
        background-color: #fff;
        color: #636b6f;
        font-family: 'Nunito', sans-serif;
        font-weight: 200;
        height: 100vh;
        margin: 0;
    }

    .full-height {
        height: 100vh;
    }

    .flex-center {
        align-items: center;
        display: flex;
        justify-content: center;
    }

    .position-ref {
        position: relative;
    }

    .top-right {
        position: absolute;
        right: 10px;
        top: 18px;
    }

    .content {
        text-align: center;
    }

    .title {
        font-size: 36px;
    }

    .links .no-button {
        color: #636b6f;
        padding: 0 25px;
        font-size: 13px;
        font-weight: 600;
        letter-spacing: .1rem;
        text-decoration: none;
        text-transform: uppercase;
        line-height: 40px;
        display: inline-block;
        border: none;
        background: none;
    }

    .m-b-md {
        margin-bottom: 30px;
    }

    label {
        font-size: 16px !important;
        font-weight: 600;
    }

    .btn-primary {
        background-color: #2ca8ff !important;
    }

    .btn:hover {
        background: white !important;
        color: #2ca8ff;
        border: 1px solid #2ca8ff;
        box-shadow: 3px 3px 5px #2ca8ff;
        transform: scale(1.1);
    }

    .btn {
        border: 1px solid #2ca8ff;
        border-radius: 0px;
        box-shadow: 3px 3px 5px #a1a1a1;
        padding: 6px 20px;
        transition: .3s all cubic-bezier(0.18, 0.89, 0.32, 1.28);
    }

    .links>a {
        color: #2ca8ff;
        padding: 0 25px;
        font-size: 13px;
        font-weight: 600;
        letter-spacing: 3px;
        text-decoration: none;
        text-transform: uppercase;
    }

    @media screen and (max-width: 426px) {
        .title {
            font-size: 20px;
        }

        .btn {
            padding: 3px 10px;
            font-size: 14px;
        }

        .links>a {
            padding: 0 10px;
            letter-spacing: 2px;
        }
    }

    @media screen and (max-width: 321px) {
        .title {
            font-size: 16px;
        }

        .btn {
            font-size: 13px;
        }

        .links>a {
            padding: 0 7px;
            letter-spacing: 1px;
        }

        .links .no-button {
            font-size: 12px;
        }
    }
    </style>
</head>

<body>
    <div class="flex-center position-ref full-height">
        @if (Route::has('login'))
        <div class="top-right links">
            @auth
            <a href="{{ url('/home') }}">Home</a>
            <a href="{{ route('logout') }}" onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                {{ __('Logout') }}
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
            @else
            <a href="{{ route('login') }}">Login</a>
            @endauth
        </div>
        @endif

        <div class="content">
            <div class="title m-b-md">
                Select the Lot or ( <button class="btn btn-primary" data-toggle="modal" data-target="#selectLot">Create
                    a New Lot</button> )
                <!-- Modal -->
                <div class="modal fade" id="selectLot" tabindex="-1" role="dialog" aria-labelledby="lotTitle"
                    aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <form action="{{ route('lot.store') }}" method="POST" enctype="multipart/formdata">
                                @csrf
                                <div class="modal-header">
                                    <h5 class="modal-title" id="lotTitle">Create a Lot</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="name">Enter the Lot Name</label>
                                        <input type="text" name="name" class="form-control" required>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="links">
                @foreach($lots as $lot)
                <form action="{{ route('lot.show', $lot->id ) }}" method="GET">
                    @csrf
                    <input type="hidden" name="lot_id" value="{{ $lot->id }}">
                    <button type="submit" class="no-button">{{ $lot->name }}</button>
                </form>
                @endforeach
            </div>
        </div>
    </div>
</body>

</html>