<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Inventory Management</title>

        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <style>
            html, body {
                background-color: #fff;
                color: #fff;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
                background-color: #2ca8ff;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 50px;
                text-transform: uppercase;
                font-weight: 600;
                letter-spacing: 15px;
                text-shadow: 2px 2px 9px #2a2a2a;
            }

            .links > a , .links > p{
                color: #efefef;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: 3px;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            @media screen and (max-width: 769px){
                .title {
                    font-size: 25px;
                }
            }
            @media screen and (max-width: 376px){
                .title {
                    font-size: 21px;
                }
            }
            @media screen and (max-width: 321px){
                .title {
                    font-size: 18px;
                }
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Inventory Management
                </div>

                <div class="links">
                    <p>Easy Way to Manage your Inventory and Geneate Profit/Loss.</p>
                </div>
                <br>
                <div class="links">
                    <p>For more Info:</p>
                    <a href="tel:+9779840465291">Contact: 9840465291</a>
                </div>
            </div>
        </div>
    </body>
</html>
