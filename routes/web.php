<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@welcome');
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');
Route::resource('lot', 'LotController')->middleware('auth');
Route::resource('sales', 'SalesController')->middleware('auth');
Route::resource('purchase', 'PurchaseController')->middleware('auth');
Route::resource('purchaseReturn', 'PurchaseReturnController')->middleware('auth');
Route::resource('salesReturn', 'SalesReturnController')->middleware('auth');
Route::resource('investor', 'InvestorController')->middleware('auth');
Route::resource('expenses', 'ExpensesController')->middleware('auth');
Route::post('/vendors/store', 'VendorController@store');
Route::post('/customers/store', 'CustomerController@store');
Route::get('profitLoss', 'HomeController@profitLoss')->middleware('auth');
Route::get('stocks', 'HomeController@stock')->middleware('auth');
Route::fallback('PageController@error');